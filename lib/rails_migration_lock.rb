module RailsMigrationLock
  class MigrationLock
    include Singleton

    def initialize
      @locked = false
    end

    def lock
      @locked ||= true
    end

    def unlock
      @locked = false
    end
  end
end
