#! /bin/bash
# entrypoint for rails application

# run any migrations
./bin/rails db:migrate

exec "$@"
