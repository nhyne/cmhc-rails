class ProtocolSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :pdf_url, :images_url

  def pdf_url
    if object.pdf.attached?
      Rails.application.routes.url_helpers.url_for(object.pdf)
    else
      nil
    end
  end

  def images_url
    images = []
    if object.pdf_images.attached?
      object.pdf_images.each do |img|
        images << Rails.application.routes.url_helpers.url_for(img)
      end
    end
    return images
  end
end
