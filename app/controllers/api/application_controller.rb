module Api
  class ApplicationController < ActionController::API
    def check_authn
      valid_token?
    end

    private

    def valid_token?
      if request.headers['Authorization'].present?
        token = request.headers['Authorization'].split(' ').last
        begin
          decoded_token = JWT.decode token, generate_public_key, true, algorithm: 'RS256'
          hopeful_issuer = (ENV['KEYCLOAK_HOST'].present? ? ENV['KEYCLOAK_HOST'] : 'cmhc-protocols.localhost') + '/auth/realms/cmhc-protocols'
          render_json_error(:forbidden) unless decoded_token[0]['iss'].split('//').last == hopeful_issuer
        rescue JWT::ExpiredSignature, JWT::DecodeError
          render_json_error(:forbidden)
        end
      else
        render_json_error(:forbidden)
      end
    end

    def generate_public_key
      verify = Rails.env == 'production'
      request_uri = request.ssl? ? 'https://' : 'http://'
      request_uri << (ENV['KEYCLOAK_HOST'].present? ? ENV['KEYCLOAK_HOST'] : 'cmhc-protocols.localhost')
      request_uri << '/auth/realms/cmhc-protocols/protocol/openid-connect/certs'
      response = HTTParty.get(request_uri, verify: verify).parsed_response
      exp = response['keys'][0]['e']
      base = response['keys'][0]['n']
      OpenSSL::PKey::RSA.new RsaPem.from(base, exp)
    end

    def render_json_error(error, message = nil)
      message ||= 'There was an error authenticating you, try logging in again.'
      render json: {
            error: message
          }.to_json, status: error
    end

  end
end
