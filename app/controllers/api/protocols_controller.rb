# Protocols Controller
module Api
  class ProtocolsController < Api::ApplicationController
    before_action :check_authn, only: %i[create update]

    def index
      render json: Protocol.all,
             each_serializer: ProtocolSerializer, steps: params[:include]
    end

    def show
      render json: Protocol.find(params[:id]), steps: params[:include]
    end

    def create
      protocol = Protocol.new(title: params[:title], description: params[:description])
      file = params['protocol_pdf']
      protocol.pdf.attach(file)
      images = Magick::ImageList.new(file.open)
      images.each_with_index do |img, i|
        partitions = file.original_filename.rpartition('.')
        file_name = "#{partitions[0]}-#{i}.png"
        file_path = Rails.root.join('tmp', file_name)
        img.write(file_path)
        protocol.pdf_images.attach(io: File.open(file_path), filename: file_name)
      end
      protocol.save
      render json: protocol
    end

    def update
      protocol = Protocol.find(params[:id])
      protocol.update(protocol_params)
      protocol.save
      render json: protocol
    end

    private

    def protocol_params
      params.require(:protocol).allow(:title, :description, :pdf)
    end
  end
end
