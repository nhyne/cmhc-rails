class Protocol < ApplicationRecord
  has_one_attached :pdf
  has_many_attached :pdf_images
end
