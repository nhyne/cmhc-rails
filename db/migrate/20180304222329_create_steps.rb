class CreateSteps < ActiveRecord::Migration[5.1]
  def change
    create_table :steps do |t|
      t.text :information, null: false
      t.integer :protocol_id, null: false
    end
  end
end
