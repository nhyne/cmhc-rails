class AddTimestamps < ActiveRecord::Migration[5.1]
  def change
    change_table :protocols do |t|
      t.timestamps
    end

    change_table :steps do |t|
      t.timestamps
    end
  end
end
