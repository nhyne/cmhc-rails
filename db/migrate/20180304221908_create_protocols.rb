class CreateProtocols < ActiveRecord::Migration[5.1]
  def change
    create_table :protocols do |t|
      t.string :title, null: false, uniq: true
      t.text :description
    end
  end
end
