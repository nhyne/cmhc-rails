class AddStepNumber < ActiveRecord::Migration[5.1]
  def change
    add_column :steps, :step_number, :integer, null: false
    add_index :steps, [:protocol_id, :step_number], unique: true
  end
end
