CmhcRails::Application.routes.default_url_options[:host] = ENV['HOST_DOMAIN'] || 'localhost:3000'
Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    resources :protocols, only: %i[index show create update]
  end
end
